package com.thouvenin_antoine.toast5stars;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    RatingBar ratBar;
    Button btnSubmit;
    TextView txvResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSubmit = findViewById(R.id.btnSubmit);
        txvResponse = findViewById(R.id.txvResponse);
        ratBar = findViewById(R.id.ratBar);

    }

    public void showResponse(View v)
    {
        float starRate = ratBar.getRating();
        String responsestring = ackRating(starRate);
        Toast.makeText(this,responsestring,Toast.LENGTH_SHORT).show();

        txvResponse.setText("you gave the application a rating of "+ starRate + " :");
    }

    public String ackRating(float s)
    {
        String ack;
        if(s >= 5.0)
        {
            ack = "Generous Greating! \n Great thanks a lot ";
        }
        else{
            if (s >= 3.0){
                ack = "ok rating";
            }
            else{
                if(s >= 2.0)
                {
                    ack="blablabla";
                }
                else{
                    if(s>=1.0){
                        ack="aegbrgilnzepgib";
                    }
                    else{
                        ack = "sdfxghjbklm256";
                    }
                }
            }
        }
        return ack;
    }
}