package com.thouvenin_antoine.contacts2020;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;
import android.content.ContentValues;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private SQLiteDatabase sdb;
    private DBOpenHelper dboh;

    private TextView txvContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // pull in the visual components from the xml file
        txvContact = findViewById(R.id.txvContact);

        // construct
        dboh = new DBOpenHelper(this, "test.db", null, 1);
        sdb = dboh.getWritableDatabase();

        // Instert a row of data into the database table
        ContentValues cv = new ContentValues();

        cv.put("Lastname", "Dubois");
        cv.put("Firstname", "Pierre");
        cv.put("Tel", "0123456789");
        cv.put("Email", "duboisp224@gmail.com");

        // insert the recotrd just created
        sdb.insert("test", null, cv);


        // prepare to do a db read
        // provide db parameters
        String table_name = "test";

        // the columns to retrieve
        String[] columns = {"ID", "Lastname", "Firstname", "Tel", "Email"};

        // the query where clause
        // we dont use it here
        String where = null;

        // guery where arguments
        // we dont use these either
        String where_args[] = null;

        // the group by clause
        // again, not used this time
        String group_by = null;

        // query having clause
        // no userd either
        String having = null;

        // query order by clause
        // not used here
        String order_by = null;

        // assign a cursor and run a query
        Cursor c = sdb.query(table_name, columns, where, where_args, group_by, having, order_by);

        String output = "Number of rows: " + c.getCount() + '\n';
        c.moveToFirst();
        for (int i = 0; i < c.getCount(); i++, c.moveToNext()) {
            output += '\n' + c.getInt(0) + '\n' +
                    c.getString(1) + ' ' +
                    c.getString(2) + '\n' +
                    c.getString(3) + '\n' +
                    c.getString(4) + "\n\n";
        }//for

        // Display the retrived records
        txvContact.setText(output);
    }//onCreate()


    // onDestroy() is an overriden method to clear the contents of the database
    // this is jsut testing so just delete everything from the table
    // dont do this in a working database environment where the loss of data
    // is potentially catastrophic

    @Override
    protected void onDestroy() {
        // call to the superclass constructor
        super.onDestroy();

        String table = "test";
        String where = null;
        String where_args[] = null;
        sdb.delete(table, where, where_args);
    }// onDestroy()
}
