package com.thouvenin_antoine.contacts2020;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;

public class DBOpenHelper extends SQLiteOpenHelper {

    public DBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(create_table);
    }

    public void onUpgrade(SQLiteDatabase db, int version_old, int version_new) {
        db.execSQL(drop_table);
        db.execSQL(create_table);
    }

    // strings for table creation
    private static final String create_table = "create table test(" +
            "ID integer primary key autoincrement," +
            "Lastname string," +
            "Firstname string," +
            "Tel string," +
            "Email string" +
            ")";

    private static final String drop_table = "drop table test";
}
