package com.thouvenin_antoine.eahello2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText edtName = (EditText) findViewById(R.id.edtName);
        Button btnHello = (Button) findViewById(R.id.btnHello);
        final TextView txvGreetings = (TextView) findViewById(R.id.txvGreetings);
        final EditText edtJob = (EditText) findViewById(R.id.edtJob);

        btnHello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (    edtJob.getText().toString().charAt(0) == 'a' ||
                        edtJob.getText().toString().charAt(0) == 'e' ||
                        edtJob.getText().toString().charAt(0) == 'i' ||
                        edtJob.getText().toString().charAt(0) == 'o' ||
                        edtJob.getText().toString().charAt(0) == 'u')
                    txvGreetings.setText("Hello " + edtName.getText().toString() + "! I see that you are an " + edtJob.getText().toString() + '.');
                else
                    txvGreetings.setText("Hello " + edtName.getText().toString() + "! I see that you are a " + edtJob.getText().toString() + '.');
            }
        });
    }
}
